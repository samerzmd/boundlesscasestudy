# Boundless Drop Recruiting
This is a test project within Boundless Drop's technical recruiting process.

## Requirements

### Technical
You need at least:

* Ruby 2.0 or later.
* A text editor (emacs, vim, nano, sublime text, notepad++)
* Some kind of shell, if you want to write unit tests.

### Knowledge

Very basic programming knowledge is assumed and no pre-knowledge of Ruby is necessary (but certainly helpful).

## The tasks
With this code you will receive a number of tasks to resolve. Each task should
not take more then 15 to 30 minutes pure working time.

### Reading JSON data files

Use the JSON facilities from the Ruby standard library to parse the provided JSON file. See file company.rb.

### Building classes from plain data

Use plain Ruby data to instantiate the user defined classes under the `src` directory. See file company.rb. You might need to implement constructors.

### Adding functionality to classes

Implement a method that would return all applicants matching a keyword. See file company.rb.

### Final result

Your final code should be able to run `ruby main.rb` and print the id and name for the correct applicants.
