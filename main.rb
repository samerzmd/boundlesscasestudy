require './src/company.rb'
require './src/applicant.rb'
require './src/job.rb'

company = Company.new('data/boundless.json')

company.find_applicants("google").each do |applicant|
  puts "Applicant Id: #{applicant.id}, Applicant Name: #{applicant.name}"
end

