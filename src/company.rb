require 'json' #include the json parser
require './src/job.rb' #include the Job class
require './src/applicant.rb' #include the Applicant class
class Company
  attr_accessor :jobs

  ## This method to loads the given JSON file into Ruby built-in data
  ## structures (hashes and arrays).
  def self.load_json(filepath)
    file = File.read (File.join(Dir.pwd, filepath))
    data = JSON.parse(file)
    return data
  end

  ## This method update the `jobs` property to an array of instances of
  ## class `Job`
  def initialize(filename)
    #region Load the json file
    data=Company.load_json(filename)
    #endregion

    #region Loop over the jobs to create an array of instance of `Job` Assign the `jobs` instance variable.
    @jobs =Array.new
    data['jobs'].each do |json_job|
      job = Job.new
      job.id=json_job['id']
      job.title=json_job['title']
      job.applicants=Array.new
      json_job['applicants'].each do |json_applicant|
        applicant=Applicant.new
        applicant.id =json_applicant['id']
        applicant.name =json_applicant['name']
        applicant.tags =json_applicant['tags']
        job.applicants.push(applicant)
      end
      @jobs.push(job)
    end
    #endregion

  end

  ## This method to return applicants from all jobs with a
  ## tag matching this keyword
  def find_applicants(keyword)
    #array of Applicants to be returned
    applicants=Array.new

    #region Use the `jobs` instance variable and search on for a match
    @jobs.each do |job|
      job.applicants.each do |applicant|
        applicant.tags.each do |tag|
          if tag==keyword
            #Ignore duplicates
            unless applicants.any? {|existed_applicants| existed_applicants.id == applicant.id}
              applicants.push(applicant)
            end
          end
        end
      end
    end
    #endregion

    #return Applicants
    applicants

  end
end







